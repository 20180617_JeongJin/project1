# Project 1 template #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code]()
* [Git repository](https://20180617_JeongJin@bitbucket.org/20180617_JeongJin/project1.git)
* [Demo video](https://youtu.be/Zw35wy2bqC8)
* Description of projects and notes in README.md (this file). 
	
	

### Description of the project

Please modify this file to include a description of your project.
You can add images here, as well as links. 
To format this document you will need to use Markdown language. Here a [tutorial](https://bitbucket.org/tutorials/markdowndemo).

Include the following things inside this file:

* briefly describe your project (objectives and what it does)
* describe how to use your project.
* list dependencies, if any (e.g., libraries you used)
* state sources of inspiration/code that you used or if anyone helped you


<br />

<br />

<br />

<br />

<br />

# 20180617 Jeong Jin Project 1 \<Idream\> #
## Concept of the project
    Idream = Idea + Stream / I + Dream
* I wanted to make a program that works like a Encyclopedia, but advanced version that suits the needs in current society
* I thought inequal level of education cause by difference of environment could be one issue that has to be solved.
* I thought people need a platform that allows them to study by themselves. Also I thought people lose interest in study because they don't know their current status/what they can study/what they have to study to study the desired conecept.
* The main function of this program is editing/studying a bubble that is connected to the existing bubble, which allows user to know his/her current status/progress in studying the subject


<br />

<br />

<br />

## Flow of the project
    This part explain overall function/usage of the program
    It doesn't show all parameters/functions, but the main important ones
  
<br />
<br />

**Press**
* program checks components of frame/etc in order and get the component that is drawn on the top, below the mouse

<br />

**Draw**
* drawing works similar to how the press works, it just shows components/etc in order so that top component is drawn on top

<br />

**Import**
* you can open map file in json format by its name
* if it's readonly, you can only read
* else, you can edit it freely

<br />

**Export**
* you can export map file in json format with new name
* if it's readonly, you can only export readonly
* else, you can export it freely

<br />

**Read**
* you can read context/edit book by pressing/hold pressing expressed unit on screen
* by pressing, you can read/whether its studied/edit page/delete unit
* by hold pressing, you can create new unit/modify connection of the unit

<br />

<br />

<br />

## Structure of the project
    This part explains the overall Class/Function and usage of them

<br />

<br />

    Global Variables

<br />

**Variables**
* boolean map_editable
    -- current file's editable
    
* boolean editable
    -- export file's editable

* boolean editable
    -- export file's editable

* Component mouse_on
    -- current component that mouse is on

* Component press_component
    -- current component that is pressed

* boolean press
    -- whether something is prssed

* int press_time
    -- pressed time

* String key_on
    -- current pressed key

* ArrayList\<Scene> scenes
    -- all scenes

* ArrayList\<Scene> current_scene
    -- current scenes

* ArrayList\<Unit> units
    -- current units

* Unit_controller unit_controller
    -- unit controller

* Text_editor text_editor
    -- text editor


<br />

**void mousePressed()**
* Does something that has to work when mouse is pressed, starts something that has to work while mouse is pressed

<br />

**void mouseReleased()**
* Does something that has to work when mouse is released, and initialize several parameters

<br />

<br />

    Unit

<br />


**Unit**
* Unit is object that stores the data of one concept. It consists of
    1. String name
    2. Book book
    3. ArrayList<Category> category
    4.  ArrayList<Unit> connection_to
    5.  ArrayList<Unit> connection_from
    6.  int x, y

<br />

**Unit_storage (extends Component)**
* Unit_storage is object that stores one Unit. It consists of
    1. Unit unit


<br />

**Unit_controller (extends Component)**
* Unit_controller is object that stores two Unit and connect/disconnect them. It consists of
    1. Unit unit_to
    2. Unit unit_from

<br />

**Unit_builder (extends Component)**
* Unit_builder is object that stores several parametes and make new unit with them. It consists of
    1. String name
    2. Book book
    3. int x, y
    4. boolean cleared
    5. ArrayList<Unit> unit_to
    6. ArrayList<Unit> unit_from

<br />

**void loadUnits (String filename)**
* It allows units saved in jason format to be loaded; converted into Unit and saved in golbal value units

<br />

**void saveUnits (String filename)**
* It allows golbal value units to be saved in jason format. When saves, boolean editable is also saved together

<br />

**void reset ()**
* It allows program to be restarted when user opens another set of units


<br />

<br />

    Component

<br />

**Interactable (interface)**
* It's for interactable ones


<br />

**Effectable (interface, extends Interactable)**
* It's for ones that can do effect on others


<br />

**Onable (interface, extends Interactable)**
* It's for ones that does something when mouse is on
    1. void if_on()

<br />

**Pressable (interface, extends Interactable)**
* It's for ones that does something when mouse is pressed
    1. void if_start_pressed()
    2. void if_while_pressed()
    3. void if_end_pressed()

<br />

**Component (abstract)**
* Component is basic class in this program. It ables other Components to be draw, stored, modified, etc.  It consists of
    1. String name
    2. int x, y
    3. color c
    4. Component reference
    5. PVector reference_delta()
    6. PGraphics get_maskimage()
    7. abstract boolean is_on()

<br />

<br />

    Frame

<br />

**Frame (abstract, extends Component)**
* Frame's main role is to store components, draw them in order, give user interaction in order
    1. int width, height

<br />

**Scene (extends Frame)**
* Scene's main role is to divide initial screen by its usage. It also re-order its component window by its interaction
    1. ArrayList\<Window\> component
    2. Window imsi_component
    3. boolean is_on()
    4. boolean is_pressed()

<br />

**Window (extends Frame)**
* Window's main role is to show components, store components.
Its the root for the componentes that does the interaction
    1. ArrayList\<Component> component
    2. ArrayList\<Component> top_component
    3. boolean touchable
    4. boolean top
    5. boolean is_on()


<br />

<br />

    Button

<br />

**Button (extends Component, implements Onable, Pressable, Effectable)**
* It's main component for input
    1. String name
    2. String name_start
    3. String name_while
    4. String name_end
    5. String name_on
    6. Component complement
    7. Button_chain chain
    8. Text text
    9. activate_chain()
    10. void button_action(String name)

<br />

**Button_square (extends Button)**
* It's square button
    1. int width, height

<br />

**Button_circle (extends Button)**
* It's circular button
    1. int diameter

<br />

**Button_chain (extends Button)**
* It's a non-shown button that ables initial visualized button to do multiple jobs at once
    1. void activated()


<br />

<br />

    Map_free

<br />

**Map_free (extends Component, implements Pressable)**
* It's component for mainscreen. It stores most of the component for the main function
    1. int width, height, current_x, current_y
    2. Unit_builder unit_builder
    3. void build_unit()
    4. void unit_sync()

<br />

**Map_free (extends Component)**
* It visualizes brief information of unit on Map_free
    1. String name
    2. String context
    3. int to, from

<br />

<br />

    Text_editor

<br />

**Text_editing (interface)**
* It's for editing text for selected text editor
    1. void signal(String signal)

<br />

**Text_editor_controller (extends Component, implements Pressable)**
* It stores text editor and initial text, which allows user to change only when its confirmed
    1. String text
    2. boolean middle
    3. boolean scrollable
    4. int current_y, width, height
    5. Text_editor editor
    6. String confirm()

<br />

**Text_editor (implements Text_editing)**
* It's an editor to change context of specific text
    1. String text
    2. String text_before, text_after
    3. boolean middle
    4. void when_signal(String signal)
    5. signal(String signal)
    6. Text_editor edit(String string)

<br />

**keyPressed ()**
* It sends current editor a signal contains pressed key
    1. String key_on
    2. text_editor.signal(key_on)

<br />

<br />

    Map Component

<br />

**Book (extends Frame, implements Pressable)**
* It contains pages, which contains an organized text in unit
    1. Book(Component reference, Unit complement)
    2. Unit complement
    3. ArrayList\<Page> pages
    4. int current_num
    5. Page current_page
    6. void remove_current()
    7. void add_previous()
    8. void add_next()
    9. void nextPage()
    10. void previousPage()


<br />

**Page (extends Frame, implements Pressable)**
* It contains part of main contexts
    1. int current_y
    2. ArrayList\<Text> page_component


<br />

<br />

    Page Component(currently, only text can be put in page)

<br />

**Page_sortable (interface)**
* It is for component that can be sorted in page
    1. void draw(int delta)

<br />

**Text (extends Component, implements Page_sortable)**
* It is component that shows the text
    1. String context
    2. String allign
    3. int size
    4. int width, height

<br />

**Line (extends Component, implements Page_sortable)**
* It is component that shows the line
    1. int x1, y1, x2, y2