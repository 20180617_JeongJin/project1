abstract class Button extends Component implements Onable, Pressable, Effectable{
  protected String name;
  protected String name_start;
  protected String name_while;
  protected String name_end;
  protected String name_on;
  protected Component complement;
  
  protected Button_chain chain;
  
  protected Text text;
  
  void add_chain(Button_chain button){
    this.chain = button;
  }
  
  void add_text(Text text){
    this.text = text;
  }
  
  Text get_text(){
    return this.text;
  }
  
  Button get_chain(){
    return this.chain;
  }
  
  void activate_chain(){
    if(chain==null) return;
    chain.activated();
  }
  
  void button_action(String name){
    if(name==null){
      
    }else if(name=="move_while_pressed"){
      //println("pressed");
      this.complement.x(this.complement.get_x()+(mouseX-pmouseX));
      this.complement.y(this.complement.get_y()+(mouseY-pmouseY));
    }else if(name=="window_pop"){
      ((Scene)complement).remove((Window)reference);
      ((Scene)complement).add((Window)reference);
    }else if(name=="close_button"){
      ((Window)complement).remove((Button)reference);
    }else if(name=="close_window"){
      //println("pressed");
      //press_able = false;
      //println(1);
      if(this.complement instanceof Window && (this.complement.get_reference() instanceof Scene || this.complement.get_reference() instanceof Window)){
        if(this.complement.get_reference() instanceof Window) ((Window)this.complement.get_reference()).remove((Window)this.complement);
        else((Scene)this.complement.get_reference()).remove((Window)this.complement);
      }
    }else if(name=="create_window"){
    
    }else if(name=="unit_while"){
      if(!map_editable) return;
      if(press_time<40 && press_time>20){
        //ellipse(x+reference_delta().x+20, y+reference_delta().y+20, (press_time-20)*2, (press_time-20)*2);
        ((Map_free)this.reference.get_reference()).add_from_0(x, y);
        
      }else if(press_time>40){
        ((Map_free)this.reference.get_reference()).add_from_1(x, y);
        //press = false;
        //press_component = null;
      }
    }else if(name=="unit_end"){
      if(((map_editable&&press_time<20) || !map_editable) && ((Window)sidebar.get(0)).get_component().size()<7){
        
        press_component = null;
        
        if(((Unit_storage)complement).get().get_book() == null) return;
        
        for(Component comp : sidebar.get(0).get_component()){
          if(((Unit_storage)complement).get().get_name().equals(((Button_square)comp).get_text().get_context())){
            return;
          }
        }
        
        Window imsi_window = new Window().x(100).y(100).c(color(255)).width(1920/2).height(1080/2).top(true).touchable(true).reference(scene_0);
        imsi_window.add(new Text(1920/2/2, 38, "<"+((Unit_storage)complement).get().get_name()+">", 30, "center", imsi_window));
        
        Book book = ((Unit_storage)complement).get().get_book();
        book.width(imsi_window.get_width()).height(imsi_window.get_height()).reference(imsi_window);
        imsi_window.add(book);
        
        if(book.get_page().size() == 0){
          Page page = new Page(imsi_window);
          page.x(0).y(0).width(1920/2-100).height(1080/2-200);
          page.add(new Text(0, 0, ((Unit_storage)complement).get().get_name(), 20, "left", page).width(1920/2-100).height(1080/2-200));
          book.add(page);
        }
        
        if(map_editable){
          Button_square button = new Button_square().x(1920/2/2-57-50).y(460).c(color(100,255,200)).width(100).height(40).name_end("unit_clear").reference(imsi_window).complement(complement);
          Text button_name0 = new Text(50, 8, "Finished", 20, "center", button);
          button.add_text(button_name0);
          imsi_window.add(button);
          
          Button_square button_erase = new Button_square().x(1920/2/2+57-50).y(460).c(color(100,255,200)).width(100).height(40).name_end("close_window").reference(imsi_window).complement(imsi_window);
          Text button_name_erase = new Text(50, 8, "Erase", 20, "center", button_erase);
          button_erase.add_text(button_name_erase);
          Button_chain chain_button_erase = new Button_chain().reference(button_erase).complement(this.complement).name("unit_erase");
         
          button_erase.add_chain(chain_button_erase);
          imsi_window.add(button_erase);
        }
        else{
          Button_square button = new Button_square().x(1920/2/2-50).y(460).c(color(100,255,200)).width(100).height(40).name_end("unit_clear").reference(imsi_window).complement(complement);
          Text button_name0 = new Text(50, 8, "Finished", 20, "center", button);
          button.add_text(button_name0);
          imsi_window.add(button);
        }
        
        
        Button_square button_a = new Button_square().x(1920/2/2-350-50).y(460).c(color(100,255,200)).width(100).height(40).name_end("previous_page").reference(imsi_window).complement(book);
        Text button_name_a = new Text(50, 8, "<|", 20, "center", button_a);
        button_a.add_text(button_name_a);
        imsi_window.add(button_a);
        
        Button_square button_b = new Button_square().x(1920/2/2+350-50).y(460).c(color(100,255,200)).width(100).height(40).name_end("next_page").reference(imsi_window).complement(book);
        Text button_name_b = new Text(50, 8, "|>", 20, "center", button_b);
        button_b.add_text(button_name_b);
        imsi_window.add(button_b);
        
        //println(map_editable);
        if(map_editable){
          Button_square button_a_add = new Button_square().x(1920/2/2-250-35).y(460).c(color(100,255,200)).width(70).height(40).name_end("add_page_previous").reference(imsi_window).complement(book);
          Text button_name_a_add = new Text(35, 8, "<|(+)", 20, "center", button_a_add);
          button_a_add.add_text(button_name_a_add);
          imsi_window.add(button_a_add);
          
          Button_square button_b_add = new Button_square().x(1920/2/2+250-35).y(460).c(color(100,255,200)).width(70).height(40).name_end("add_page_next").reference(imsi_window).complement(book);
          Text button_name_b_add = new Text(35, 8, "(+)|>", 20, "center", button_b_add);
          button_b_add.add_text(button_name_b_add);
          imsi_window.add(button_b_add);
          Button_square button_c_add = new Button_square().x(1920/2/2-158-35).y(460).c(color(100,255,200)).width(70).height(40).name_end("edit_page_current").reference(imsi_window).complement(book);
          Text button_name_c_add = new Text(35, 8, "Edit", 20, "center", button_c_add);
          button_c_add.add_text(button_name_c_add);
          imsi_window.add(button_c_add);
          
          Button_square button_c_delete = new Button_square().x(1920/2/2+158-35).y(460).c(color(100,255,200)).width(70).height(40).name_end("delete_page_current").reference(imsi_window).complement(book);
          Text button_name_c_delete = new Text(35, 8, "Delete", 20, "center", button_c_delete);
          button_c_delete.add_text(button_name_c_delete);
          imsi_window.add(button_c_delete);
        }
        
        scene_0.add(imsi_window);
        
        Button_chain chain = new Button_chain().reference(imsi_window).complement(imsi_window.get_reference()).name("window_pop");
        Button_square button1 = new Button_square().x(100-70).y(30 + 70* sidebar.get(0).get_component().size()).c(color(200,200,255)).width(140).height(40).name_end("").reference(sidebar.get(0)).complement(imsi_window);
        Text button_name = new Text(70, 8, ((Unit_storage)complement).get().get_name(), 20, "center", button1);
        button_name.c(color(0));
        button1.add_text(button_name);
        button1.add_chain(chain);
        
        Button_chain chain0 = new Button_chain().reference(button1).complement(button1.get_reference()).name("close_button");
        Button_chain chain1 = new Button_chain().reference(button1).complement(button1.get_reference()).name("button_organize");
        chain0.add_chain(chain1);
        sidebar.get(0).add(button1);
        
        ((Button)imsi_window.get_top_component(1)).add_chain(chain0);
      }else if(press_time<40){
        ArrayList<Component> imsi = new ArrayList<Component>();
        ((Window)((Map_free)((Window)this.reference).reference).get(3)).component(imsi);
        ((Map_free)((Window)this.reference).reference).reset_unit_builder();
        //press = false;
        //press_component = null;
      }else{
        ((Map_free)this.reference.get_reference()).add_from_2(complement);
        //press = false;
        //press_component = null;
      }
    }else if(name=="unit_on"){
      //if((this.reference_delta().x+x+x_pos-mouseX)*(this.reference_delta().x+x+x_pos-mouseX)+
      //  (this.reference_delta().y+y+y_pos-mouseY)*(this.reference_delta().y+y+y_pos-mouseY)<20*20){
      //    ((Window)this.get(2)).add(new Unit_guide(unit.get_name(), unit.get_context(), x_pos, y_pos, unit.get_connection_to().size(), unit.get_connection_from().size(),(Window)this.get(2)));
      //}
    }else if(name=="unit_clear"){
      //println(1);
      //println(((Unit_storage)complement).get().get_cleared());
      ((Unit_storage)complement).get().cleared(!((Unit_storage)complement).get().get_cleared());
      //println(((Unit_storage)complement).get().get_cleared());
    }else if(name == "unit_confirm"){
      ((Map_free)this.complement).build_unit();
    }else if(name == "text_confirm"){
      ((Text_editor_controller)this.complement).confirm();
      //println(1);
    }else if(name == "unit_name"){
      ((Map_free)this.complement).add_from_3(((Text_editor_controller)this.reference).confirm());
    }else if(name == "button_organize"){
      ArrayList <Component> component = ((Window)this.complement).get_component();
      for(int i=0; i<component.size(); i++){
        component.get(i).y(20+i*70);
      }
      //println(2);
    }else if(name == "next_page"){
      ((Book)this.complement).nextPage();
    }else if(name == "previous_page"){
      ((Book)this.complement).previousPage();
    }else if(name == "window_save"){
      Window imsi_window = new Window().x(0).y(0).c(color(255,100)).width(displayWidth-200).height(displayHeight).close(true).touchable(true).reference(scene_0);
      Text_editor_controller text_edit_controller;
      text_edit_controller = new Text_editor_controller().x(imsi_window.get_width()/2-150).y(imsi_window.get_height()/2-150).width(300).height(300).reference(imsi_window).compliment(null);
      
      //unit_builder.name("11");
      imsi_window.add(text_edit_controller);
      Button_square imsi_button = new Button_square().x(imsi_window.get_width()/2-50).y(imsi_window.get_height()/2+130-20).c(color(150,255,200)).width(100).height(30).name_end("").reference(imsi_window).complement(text_edit_controller);
      Text button_name = new Text(50, 4, "Confirm", 20, "center", imsi_button);
      imsi_button.add_text(button_name);
      Button_chain chain0 = new Button_chain().name("save").complement(null).reference(text_edit_controller);
      Button_chain chain1 = new Button_chain().name("").reference(null).complement(null);
      Button_chain chain2 = new Button_chain().name("close_window").reference(imsi_button).complement(imsi_window);
      imsi_button.add_chain(chain0);
      chain0.add_chain(chain1);
      chain1.add_chain(chain2);
      imsi_window.add(imsi_button);
      imsi_window.add(new Text(imsi_window.get_width()/2, imsi_window.get_height()/2-120, "Enter New Map Name", 20, "center", imsi_window));
      (scene_0).add(imsi_window);
    }else if(name == "window_load"){
      Window imsi_window = new Window().x(0).y(0).c(color(255,100)).width(displayWidth-200).height(displayHeight).close(true).touchable(true).reference(scene_0);
      Text_editor_controller text_edit_controller;
      text_edit_controller = new Text_editor_controller().x(imsi_window.get_width()/2-150).y(imsi_window.get_height()/2-150).width(300).height(300).reference(imsi_window).compliment(null);
      
      //unit_builder.name("11");
      imsi_window.add(text_edit_controller);
      Button_square imsi_button = new Button_square().x(imsi_window.get_width()/2-50).y(imsi_window.get_height()/2+130-20).c(color(150,255,200)).width(100).height(30).name_end("").reference(imsi_window).complement(text_edit_controller);
      Text button_name = new Text(50, 4, "Confirm", 20, "center", imsi_button);
      imsi_button.add_text(button_name);
      Button_chain chain0 = new Button_chain().name("load").complement(null).reference(text_edit_controller);
      Button_chain chain1 = new Button_chain().name("").reference(null).complement(null);
      Button_chain chain2 = new Button_chain().name("close_window").reference(imsi_button).complement(imsi_window);
      imsi_button.add_chain(chain0);
      chain0.add_chain(chain1);
      chain1.add_chain(chain2);
      imsi_window.add(imsi_button);
      imsi_window.add(new Text(imsi_window.get_width()/2, imsi_window.get_height()/2-120, "Enter Map Name To Load", 20, "center", imsi_window));
      (scene_0).add(imsi_window);
    }else if(name == "save"){
      saveUnits(((Text_editor_controller)this.reference).confirm());
    }else if(name == "load"){
      reset();
      loadUnits(((Text_editor_controller)this.reference).confirm());
      
      if(!map_editable) editable = false;
    
      String editable_string = "";
      color c;
      if(editable){
        editable_string = "Export : Editable";
        c = color(150,255,200);
      }else{
        editable_string = "Export : ReadOnly";
        c = color(255,150,200);
      }
      Button_square savemode_button = new Button_square();
      savemode_button.x(200/2-85).y(210 + 50).c(c).width(170).height(30).name_end("editmode").reference(profile.get(0)).complement(savemode_button);
      Text button_name_savemode = new Text(85, 6, editable_string, 15, "center", savemode_button);
      savemode_button.add_text(button_name_savemode);
      profile.get(0).add(savemode_button);
      
      String map_editable_string = "";
      color map_c;
    
      if(map_editable){
        map_editable_string = "Map : Editable";
        map_c = color(150,255,200);
      }
      else{
        map_editable_string = "Map : ReadOnly";
        map_c = color(255,150,200);
      }
      
      Button_square map_savemode_button = new Button_square();
      map_savemode_button.x(200/2-85).y(210-45 + 50).c(map_c).width(170).height(30).name_end("").reference(profile.get(0)).complement(null);
      Text map_button_name_savemode = new Text(85, 6, map_editable_string, 15, "center", map_savemode_button);
      map_savemode_button.add_text(map_button_name_savemode);
      profile.get(0).add(map_savemode_button);
      
      Button_square current_name = new Button_square().x(200/2 -85).y(120 + 50).c(color(255)).width(170).height(30).name_end("").reference(profile.get(0)).complement(null);
      Text current_name_text = new Text(85, 6, current_map_name, 15, "center", current_name);
      current_name.add_text(current_name_text);
      profile.get(0).add(current_name);
      
    }else if(name == "editmode"){
      if(map_editable) editable = !editable;
      if(!map_editable) editable = false;
      String editable_string = "";
      color c;
      if(editable){
        editable_string = "Export : Editable";
        c = color(150,255,200);
      }else{
        editable_string = "Export : ReadOnly";
        c = color(255,150,200);
      }
      Text button_name_savemode = new Text(85, 6, editable_string, 15, "center", complement);
      ((Button_square)complement).c(c).add_text(button_name_savemode);
    }else if(name == "add_page_previous"){
      ((Book)complement).add_previous();
    }else if(name == "add_page_next"){
      ((Book)complement).add_next();
    }else if(name == "edit_page_current"){
      Window imsi_window = new Window().x(0).y(0).c(color(255,100)).width(displayWidth-200).height(displayHeight).close(true).touchable(true).reference(scene_0);
      Text_editor_controller text_edit_controller;
      text_edit_controller = new Text_editor_controller().x(imsi_window.get_width()/2-150).y(imsi_window.get_height()/2-150).width(300).height(300).reference(imsi_window).compliment(null).middle(false);
      //println(text_edit_controller.get_editor().text);
      text_edit_controller.get_editor().text(((Book)complement).get_current_page().get_page_component().get(0).get_context());
      text_edit_controller.scrollable(true);
      
      //println(text_edit_controller.get_editor().text);
      imsi_window.add(text_edit_controller);
      Button_square imsi_button = new Button_square().x(imsi_window.get_width()/2-50).y(imsi_window.get_height()/2+130-20).c(color(150,255,200)).width(100).height(30).name_end("").reference(imsi_window).complement(null);
      Text button_name = new Text(50, 4, "Confirm", 20, "center", imsi_button);
      imsi_button.add_text(button_name);
      Button_chain chain0 = new Button_chain().name("edit_confirm").complement(((Book)complement).get_current_page()).reference(text_edit_controller);
      //Button_chain chain1 = new Button_chain().name("unit_confirm").reference(imsi_button).complement(this);
      Button_chain chain2 = new Button_chain().name("close_window").reference(imsi_button).complement(imsi_window);
      imsi_button.add_chain(chain0);
      //chain0.add_chain(chain1);
      //chain1.add_chain(chain2);
      chain0.add_chain(chain2);
      imsi_window.add(imsi_button);
      imsi_window.add(new Text(imsi_window.get_width()/2, imsi_window.get_height()/2-120, "Edit Context", 20, "center", imsi_window));
      (scene_0).add(imsi_window);
    }else if(name == "delete_page_current"){
      ((Book)complement).remove_current();
    }else if(name == "edit_confirm"){
      ((Page)complement).get_page_component().get(0).context(((Text_editor_controller)reference).confirm());
    }else if(name == "unit_erase"){
      Unit unit = ((Unit_storage)complement).get();
      units.remove(unit);
      
      Component component_imsi = null;
      
      for(Component comp: sidebar.get(0).get_component()){
        if(((Button_square)comp).get_text().get_context().equals(((Unit_storage)this.complement).get().get_name())){
          component_imsi = comp;
        }
      }
      
      ((Window)component_imsi.get_reference()).remove(component_imsi);
      
      ArrayList <Component> component = ((Window)component_imsi.get_reference()).get_component();
      for(int i=0; i<component.size(); i++){
        component.get(i).y(20+i*70);
      }
      
      unit_controller = new Unit_controller();
      
      ArrayList<Unit> imsi_connection_to = new ArrayList<Unit>();
      for(Unit connection_to: unit.get_connection_to()){
        imsi_connection_to.add(connection_to);
      }
      
      for(Unit connection_to: imsi_connection_to){
        unit_controller.unit_to(connection_to);
        unit_controller.unit_from(unit);
        unit_controller.disconnect();
      }
      
      ArrayList<Unit> imsi_connection_from = new ArrayList<Unit>();
      for(Unit connection_from: unit.get_connection_from()){
        imsi_connection_from.add(connection_from);
      }
      
      for(Unit connection_from: imsi_connection_from){
        unit_controller.unit_to(unit);
        unit_controller.unit_from(connection_from);
        unit_controller.disconnect();
      }
      unit_controller = new Unit_controller();
    }
  }
  
  Component get_complement(){
    return complement;
  }
}

class Button_square extends Button{
  protected int width, height;
  
  Button_square(){
    
  }
  
  Button_square x(int x){
    this.x = x;
    return this;
  }
  
  Button_square y(int y){
    this.y = y;
    return this;
  }
  
  Button_square c(color c){
    this.c = c;
    return this;
  }
  
  Button_square width(int width){
    this.width = width;
    return this;
  }
  
  Button_square height(int height){
    this.height = height;
    return this;
  }
  
  Button_square reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Button_square complement(Component complement){
    this.complement = complement;
    return this;
  }
  
  Button_square name(String name){
    this.name = name;
    return this;
  }
  
  Button_square name_on(String name_on){
    this.name_on = name_on;
    return this;
  }
  
  Button_square name_start(String name_start){
    this.name_start = name_start;
    return this;
  }
  
  Button_square name_while(String name_while){
    this.name_while = name_while;
    return this;
  }
  
  Button_square name_end(String name_end){
    this.name_end = name_end;
    return this;
  }
  
  void draw(){    
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    //println(maskImage.width, maskImage.height);
    
    hint(ENABLE_DEPTH_TEST);
    if(maskImage==null) return;
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.fill(this.c);
    sourceImage.rect(x, y, width, height);
    sourceImage.endDraw();
    
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
    
    if(text!=null) text.draw();
  }
  
  void if_start_pressed(){
    //current_pressed = myself();
    //println(3);
    button_action(name_start);
  }
  
  void if_while_pressed(){
    //current_pressed = myself();
    //println(3);
    button_action(name_while);
  }
  
  void if_end_pressed(){
    //current_pressed = myself();
    //println(3);
    button_action(name_end);
    activate_chain();
  }
  
  boolean is_on(){
    if(mouseX>this.reference_delta().x+x && mouseX<this.reference_delta().x+x+width
    && mouseY>this.reference_delta().y+y && mouseY<this.reference_delta().y+y+height){
      mouse_on = this;
      if_on();
      return true;
    }
    return false;
  }
  
  void if_on(){
    button_action(name_on);
  }
  
  int get_width(){
    return width;
  }
  
  int get_height(){
    return height;
  }
}

class Button_circle extends Button{
  protected int diameter;
  
  Button_circle(){
    
  }
  
  Button_circle x(int x){
    this.x = x;
    return this;
  }
  
  Button_circle y(int y){
    this.y = y;
    return this;
  }
  
  Button_circle c(color c){
    this.c = c;
    return this;
  }
  
  Button_circle diameter(int diameter){
    this.diameter = diameter;
    return this;
  }
  
  Button_circle reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Button_circle complement(Component complement){
    this.complement = complement;
    return this;
  }
  
  Button_circle name(String name){
    this.name = name;
    return this;
  }
  
  Button_circle name_on(String name_on){
    this.name_on = name_on;
    return this;
  }
  
  Button_circle name_start(String name_start){
    this.name_start = name_start;
    return this;
  }
  
  Button_circle name_while(String name_while){
    this.name_while = name_while;
    return this;
  }
  
  Button_circle name_end(String name_end){
    this.name_end = name_end;
    return this;
  }
  
  void draw(){    
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    //println(maskImage.width, maskImage.height);
    
    hint(ENABLE_DEPTH_TEST);
    if(maskImage==null) maskImage = createGraphics(0, 0);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.fill(this.c);
    sourceImage.ellipseMode(CORNER);
    sourceImage.ellipse(x, y, diameter, diameter);
    sourceImage.endDraw();
    
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
  }
  
  void if_start_pressed(){
    //current_pressed = myself();
    //println(3);
    button_action(name_start);
  }
  
  void if_while_pressed(){
    //current_pressed = myself();
    //println(3);
    button_action(name_while);
  }
  
  void if_end_pressed(){
    //current_pressed = myself();
    //println(3);
    button_action(name_end);
    activate_chain();
  }
  
  boolean is_on(){
    if((mouseX-this.reference_delta().x-x-diameter/2)*(mouseX-this.reference_delta().x-x-diameter/2)+
    (mouseY-this.reference_delta().y-y-diameter/2)*(mouseY-this.reference_delta().y-y-diameter/2)<diameter*diameter/4){
      mouse_on = this;
      if_on();
      return true;
    }
    return false;
  }
  
  void if_on(){
    button_action(name_on);
  }
}

class Button_chain extends Button{
  String name;
  
  Button_chain name(String name){
    this.name = name;
    return this;
  }
  
  void activated(){
    button_action(name);
    activate_chain();
  }
  
  boolean is_on(){
    return false;
  }
  
  Button_chain reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Button_chain complement(Component complement){
    this.complement = complement;
    return this;
  }
  
  void if_start_pressed(){
    
  }
  
  void if_on(){
    
  }
  
  void if_while_pressed(){
  
  }
  
  void if_end_pressed(){
    
  }
  
  void draw(){
    
  }
}
