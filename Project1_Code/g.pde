interface Text_editing{
  void signal(String signal);
}

class Text_editor_controller extends Component implements Pressable{
  protected String text;
  protected boolean editable;
  protected boolean oneline;
  protected boolean middle;
  
  protected boolean scrollable;
  protected int current_y;
  
  protected int width;
  protected int height;
  
  protected Window window;
  protected Text_editor editor;
  
  protected String complement;
  
  //String text(){
    
  //}
  
  Text_editor_controller scrollable(boolean scrollable){
    this.scrollable = scrollable;
    return this;
  }
  
  Text_editor get_editor(){
    return this.editor;
  }
  
  String confirm(){
    this.text = editor.get_text();
    editor.text(this.text);
    
    complement = this.text;
    return this.text;
  }
  
  String get_complement(){
    return this.complement;
  }
  
  Text_editor_controller compliment(String complement){
    this.complement = complement;
    return this;
  }
  
  Text_editor_controller middle(boolean middle){
    this.middle = middle;
    return this;
  }
  
  Text_editor_controller x(int x){
    this.x = x;
    return this;
  }
  
  Text_editor_controller y(int y){
    this.y = y;
    return this;
  }
  
  Text_editor_controller width(int width){
    this.width = width;
    return this;
  }
  
  Text_editor_controller height(int height){
    this.height = height;
    return this;
  }
  
  Text_editor_controller reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Text_editor_controller(){
    text = "";
    editable = true;
    editor = new Text_editor();
    window = new Window();
    middle = true;
    oneline = false;
    scrollable = false;
    current_y = 0;
  }
  
  boolean is_on(){
    //println(1);
    if(mouseX>this.reference_delta().x+x && mouseX<this.reference_delta().x+x+width
    && mouseY>this.reference_delta().y+y && mouseY<this.reference_delta().y+y+height){
      mouse_on = this;
      return true;
    }
    return false;
  }
  
  void if_start_pressed(){
    //println(1);
    text_editor = this.editor;
  }
  
  void if_while_pressed(){
    if(scrollable) current_y += mouseY-pmouseY;
    if(current_y>0) current_y=0;
  }
  
  void if_end_pressed(){
    
  }
  
  void draw(){
    PGraphics sourceImage;
    //println(editable);
    sourceImage = createGraphics(width+1, height+1);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.background(255);
    sourceImage.rect(0,0,width,height);
    sourceImage.fill(0);
    
    //println(editor.get_text());
    
    if(middle){
      sourceImage.textAlign(CENTER);
      sourceImage.text(editor.get_text_before() + "|" + editor.get_text_after(), 20, 70 + current_y, width-40, height - current_y);
    }else{
      sourceImage.textAlign(LEFT);
      sourceImage.text(editor.get_text_before() + "|" + editor.get_text_after(), 20, 70 + current_y, width-40, height - current_y);
    }
    sourceImage.stroke(0);
    sourceImage.fill(255);
    sourceImage.rect(0,0,width, 70);
    sourceImage.rect(0,height-70,width, 70);
    sourceImage.endDraw();
    image(sourceImage, this.reference_delta().x+x, this.reference_delta().y+y);
  }
}

class Text_editor implements Text_editing{
  protected String text;
  protected boolean editable;
  protected int curser;
  protected String text_before;
  protected String text_after;
  
  protected boolean oneline;
  protected boolean middle;
  
  String get_text(){
    return text;
  }
  
  String get_text_before(){
    text_before = text.substring(0, curser);
    text_after = text.substring(curser, text.length());
    return text_before;
  }
  
  String get_text_after(){
    text_before = text.substring(0, curser);
    text_after = text.substring(curser, text.length());
    return text_after;
  }
  
  void when_signal(String signal){
    if(!editable) return;
    this.edit(signal);
  }
  
  void signal(String signal){
    when_signal(signal);
  }
  
  Text_editor(){
    //println(1);
    text = "write here";
    
    editable = true;
    oneline = false;
    curser = text.length();
    
    text_before = text.substring(0, curser);
    text_after = text.substring(curser, text.length());
  }
  
  Text_editor text(String text){
    this.text = text;
    curser = text.length();
    return this;
  }
  
  Text_editor editable(boolean editable){
    this.editable = editable;
    return this;
  }
  
  Text_editor edit(String string){
    String key_on = string;
    
    //println(key_on);
    
    if(key_on.equals("up")){
      key_on = "";
    }
    if(key_on.equals("down")){
      key_on = "";
    }
    if(key_on.equals("left")){
      if(text_before.length()>0)curser-=1;
      key_on = "";
    }
    if(key_on.equals("right")){
      if(text_before.length()<text.length())curser+=1;
      key_on = "";
    }
    if(key_on.equals("backspace")){
      if(text_before.length()>0){
        text_before=text_before.substring(0, text_before.length()-1);
        curser -= 1;
        text = text_before + text_after;
      }
      key_on = "";
    }
    if(key_on.equals("enter")){
      if(!oneline){
        text_before += "\n";
        text = text_before + text_after;
        curser += 1;
        key_on = "";
      }
    }
    if(key_on.equals("shift")){
      key_on = "";
    }
    if(!key_on.equals("")){
      text_before += key_on;
      text = text_before + text_after;
      curser += 1;
      key_on = "";
    }
    
    text_before = text.substring(0, curser);
    text_after = text.substring(curser, text.length());
    return this;
  }
}

void keyPressed(){
  //println();
  String key_on = "";
  
  if(key == CODED){
    if(keyCode == LEFT){
      key_on = "left";
    }
    if(keyCode == RIGHT){
      key_on = "right";
    }
    if(keyCode == UP){
      key_on = "up";
    }
    if(keyCode == DOWN){
      key_on = "down";
    }
    if(keyCode == SHIFT){
      key_on = "shift";
    }
  }
  
  if(key == ESC){
    key_on = "esc";
  }
  if(key == ENTER){
    key_on = "enter";
  }
  if(key == BACKSPACE){
    key_on = "backspace";
  }
  
  if(key_on.equals("")) key_on = ""+key;
  
  text_editor.signal(key_on);
}

void keyReleased(){
  
}
