abstract class Frame extends Component{
  protected int width;
  protected int height;

  int get_width(){
    return width;
  }
  
  int get_height(){
    return height;
  }
}

class Scene extends Frame{
  protected ArrayList<Window> component;
  
  protected Window imsi_component;
  
  Scene(){
    component = new ArrayList<Window> ();
    imsi_component = null;
  }
  
  Scene imsi_component(Window imsi_component){
    this.imsi_component = imsi_component;
    return this;
  }
  
  Scene c(color c){
    this.c = c;
    return this;
  }
  
  Scene x(int x){
    this.x = x;
    return this;
  }
  
  Scene y(int y){
    this.y = y;
    return this;
  }
  
  Scene reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Scene width(int width){
    this.width = width;
    return this;
  }
  
  Scene height(int height){
    this.height = height;
    return this;
  }
  
  Scene add(Window component){
    this.component.add(component);
    return this;
  }
  
  Scene remove(Window component){
    this.component.remove(component);
    return this;
  }
  
  void draw(){
    for(int i=0; i<component.size(); i++){
      component.get(i).draw();
    }
  }
  
  boolean is_pressed(){
    boolean on = false;
    Window on_window = null;
    
    if(mouseX>this.reference_delta().x+x && mouseX<this.reference_delta().x+x+width
    && mouseY>this.reference_delta().y+y && mouseY<this.reference_delta().y+y+height){
      for(int i=0; i<component.size(); i++){
        on_window = component.get(component.size()-i-1);
        on = component.get(component.size()-i-1).is_on();
        if(on){
          component.remove(on_window);
          component.add(on_window);   
          return true;
        }
      }
      mouse_on = this;
      return true;
    }
    return false;
  }
  
  boolean is_on(){
    boolean on = false;
    Window on_window = null;
    
    if(mouseX>this.reference_delta().x+x && mouseX<this.reference_delta().x+x+width
    && mouseY>this.reference_delta().y+y && mouseY<this.reference_delta().y+y+height){
      for(int i=0; i<component.size(); i++){
        on_window = component.get(component.size()-i-1);
        on = component.get(component.size()-i-1).is_on();
        if(on){ 
          return true;
        }
      }
      mouse_on = this;
      return true;
    }
    return false;
  }
  
  Window get(int n){
    if(component.size()==0) return null;
    return component.get(n);
  }
}

class Window extends Frame{
  protected ArrayList<Component> component;
  protected ArrayList<Component> top_component;
  protected boolean touchable;
  protected boolean top;
  
  Window(){
    component = new ArrayList<Component> ();
    top_component = new ArrayList<Component> ();
    //top_component.add(new Window().x(0).y(0).width(width).height(height).c(color(0,0)).touchable(false));
  }
  
  Window c(color c){
    this.c = c;
    return this;
  }
  
  Window x(int x){
    this.x = x;
    return this;
  }
  
  Window y(int y){
    this.y = y;
    return this;
  }
  
  Window top(boolean top){
    this.top = top;
    if(top){
      top_component.add(new Button_square().x(0).y(0).c(color(150)).width(width).height(30).name_while("move_while_pressed").reference(this).complement(this));
      top_component.add(new Button_circle().x(5).y(5).c(color(255,0,0)).diameter(20).name_end("close_window").reference(this).complement(this));
    }
    return this;
  }
  
  Window close(boolean close){
    if(close){
      top_component.add(new Button_circle().x(5).y(5).c(color(255,0,0)).diameter(20).name_end("close_window").reference(this).complement(this));
    }
    return this;
  }
  
  Window touchable(boolean touchable){
    this.touchable = touchable;
    return this;
  }
  
  Window reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Window width(int width){
    this.width = width;
    return this;
  }
  
  Window height(int height){
    this.height = height;
    return this;
  }
  
  Window add(Component component){
    this.component.add(component);
    return this;
  }
  
  Window remove(Component component){
    this.component.remove(component);
    return this;
  }
  
  Window component(ArrayList<Component> component){
    this.component = component;
    return this;
  }
  
  Component get_top_component(int n){
    return top_component.get(n);
  }
  
  void draw(){
    //rectMode(CENTER);
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    hint(ENABLE_DEPTH_TEST);
    if(maskImage==null) maskImage = createGraphics(displayWidth, displayHeight);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.fill(this.c);
    sourceImage.rect(x, y, width, height);
    sourceImage.endDraw();
    
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
    
    for(int i=0; i<component.size(); i++){
      component.get(i).draw();
    }
    
    for(int i=0; i<top_component.size(); i++){
      top_component.get(i).draw();
    }
  }
  
  ArrayList<Component> get_component(){
    return this.component;
  }
  
  Component get(int n){
    if(component.size()==0) return null;
    return component.get(n);
  }
  
  void if_pressed(){
    
  }
  
  boolean is_on(){
    if(mouseX>this.reference_delta().x+x && mouseX<this.reference_delta().x+x+width
    && mouseY>this.reference_delta().y+y && mouseY<this.reference_delta().y+y+height){
      for(int i=0; i<top_component.size(); i++){
        boolean imsi = top_component.get(top_component.size()-i-1).is_on();
        if(imsi) return imsi;
      }
      
      for(int i=0; i<component.size(); i++){
        boolean imsi = component.get(component.size()-i-1).is_on();
        if(imsi) return imsi;
      }
      mouse_on = this;
      return touchable;
    }
    return false;
  }
  
  int get_width(){
    return width;
  }
  
  int get_height(){
    return height;
  }
}
