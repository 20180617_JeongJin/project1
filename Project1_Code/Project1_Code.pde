String current_map_name;

boolean map_editable;
boolean editable;

Component mouse_on;
String key_on;

Component press_component;
boolean press;
int press_time;

ArrayList<Scene> scenes;
ArrayList<Scene> current_scene;
Scene scene_0;
Scene sidebar;
Scene profile;

ArrayList<Unit> units;
Unit_controller unit_controller;

Text_editor text_editor = new Text_editor();
//Text_editor_controller c = new Text_editor_controller();

void setup(){
  size(1440, 900);
  
  current_map_name = "Map Name";
  
  map_editable = false;
  editable = false;
  
  units = new ArrayList<Unit>();
  unit_controller = new Unit_controller();
  
  scenes = new ArrayList<Scene>();
  current_scene = new ArrayList<Scene>();
  
  scene_0 = new Scene().x(200).y(0).c(color(255)).width(displayWidth-200).height(displayHeight);
  //Window main_window = new Window().x(100).y(100).c(color(255)).width(1920/2).height(1080/2).top(true).touchable(true).reference(scene_0);
  //main_window.add(new Map_free().x(0).y(30).c(color(255)).width(main_window.get_width()).height(main_window.get_height()-30).reference(main_window).grid(true));
  Window main_window = new Window().x(0).y(0).c(color(255)).width(displayWidth-200).height(displayHeight).top(false).touchable(true).reference(scene_0);
  main_window.add(new Map_free().x(0).y(0).c(color(255)).width(displayWidth-200).height(displayHeight).reference(main_window).grid(true));
  scene_0.add(main_window);
  
  //main_window.add(c.x(100).y(100).width(300).height(300).reference(main_window));
  
  sidebar = new Scene().x(0).y(350).c(color(255)).width(201).height(displayHeight-350);
  Window sidebar_window = new Window().x(0).y(0).c(color(100)).width(200).height(displayHeight-350).top(false).touchable(true).reference(sidebar);
  sidebar.add(sidebar_window);
  
  profile = new Scene().x(0).y(0).c(color(255)).width(201).height(350);
  Window profile_default = new Window().x(0).y(0).c(color(150)).width(200).height(350).top(false).touchable(true).reference(profile);
  Button_square save_button = new Button_square().x(200/2 + 45 -40).y(255 + 50).c(color(150,255,200)).width(80).height(30).name_end("window_save").reference(profile_default).complement(null);
  Button_square load_button = new Button_square().x(200/2 - 45 -40).y(255 + 50).c(color(150,255,200)).width(80).height(30).name_end("window_load").reference(profile_default).complement(null);
  Text button_name_save = new Text(40, 4, "Save", 20, "center", save_button);
  save_button.add_text(button_name_save);
  Text button_name_load = new Text(40, 4, "Load", 20, "center", load_button);
  load_button.add_text(button_name_load);
  profile_default.add(save_button);
  profile_default.add(load_button);
  
  Button_square current_name = new Button_square().x(200/2 -85).y(120 + 50).c(color(255)).width(170).height(30).name_end("").reference(profile_default).complement(null);
  Text current_name_text = new Text(85, 6, current_map_name, 15, "center", current_name);
  current_name.add_text(current_name_text);
  profile_default.add(current_name);
  
  String editable_string = "";
  color c;
  if(editable){
    editable_string = "Export : Editable";
    c = color(150,255,200);
  }
  else{
    editable_string = "Export : ReadOnly";
    c = color(255,150,200);
  }
  
  Button_square savemode_button = new Button_square();
  savemode_button.x(200/2-85).y(210 + 50).c(c).width(170).height(30).name_end("editmode").reference(profile_default).complement(savemode_button);
  Text button_name_savemode = new Text(85, 6, editable_string, 15, "center", savemode_button);
  savemode_button.add_text(button_name_savemode);
  profile_default.add(savemode_button);
  
  String map_editable_string = "";
  color map_c;
  if(map_editable){
    map_editable_string = "Map : Editable";
    map_c = color(150,255,200);
  }
  else{
    map_editable_string = "Map : ReadOnly";
    map_c = color(255,150,200);
  }
  Button_square map_savemode_button = new Button_square();
  map_savemode_button.x(200/2-85).y(210-45 + 50).c(map_c).width(170).height(30).name_end("").reference(profile_default).complement(null);
  Text map_button_name_savemode = new Text(85, 6, map_editable_string, 15, "center", map_savemode_button);
  map_savemode_button.add_text(map_button_name_savemode);
  profile_default.add(map_savemode_button);
  
  profile.add(profile_default);
  
  current_scene.add(scene_0);
  current_scene.add(sidebar);
  current_scene.add(profile);
  
  load_button.if_end_pressed();
}

void draw(){
  background(100);
  
  for(Scene scene: current_scene){
    scene.draw();
    scene.is_on();
  }

  if(mouse_on instanceof Onable){
    ((Onable)mouse_on).if_on();
  }
  
  if(press) press_time +=1 ;
  if(press_component instanceof Pressable){
    ((Pressable)press_component).if_while_pressed();
  }
}

void mousePressed(){
  press_time = 0;
  text_editor = new Text_editor();
  
  if(mouse_on!=null && mouse_on instanceof Pressable) ((Pressable)mouse_on).if_start_pressed();
  
  press = true;
  press_component = mouse_on;
  
  for(int i=0; i<current_scene.size();i++){
    current_scene.get(current_scene.size()-i-1).is_pressed();
  }
}

void mouseReleased(){
  if(press_component!=null && press_component instanceof Pressable) ((Pressable)press_component).if_end_pressed();
  
  press = false;
  press_component = null;
}
