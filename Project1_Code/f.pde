//class Profile{
//  private String name;
//  private String id;
//  private String passward;
//}

void reset(){
  units = new ArrayList<Unit>();
  unit_controller = new Unit_controller();
  
  //Unit unit;
  //unit = new Unit().name("Start").x(2).y(4); //Start:0
  //units.add(unit);
  //unit = new Unit().name("A").x(6).y(3); //A:1
  //units.add(unit);
  //unit_controller.unit_from(units.get(0)).unit_to(units.get(1)).connect();
  
  scenes = new ArrayList<Scene>();
  current_scene = new ArrayList<Scene>();
  
  scene_0 = new Scene().x(200).y(0).c(color(255)).width(displayWidth-200).height(displayHeight);
  //Window main_window = new Window().x(100).y(100).c(color(255)).width(1920/2).height(1080/2).top(true).touchable(true).reference(scene_0);
  //main_window.add(new Map_free().x(0).y(30).c(color(255)).width(main_window.get_width()).height(main_window.get_height()-30).reference(main_window).grid(true));
  Window main_window = new Window().x(0).y(0).c(color(255)).width(displayWidth-200).height(displayHeight).top(false).touchable(true).reference(scene_0);
  main_window.add(new Map_free().x(0).y(0).c(color(255)).width(displayWidth-200).height(displayHeight).reference(main_window).grid(true));
  scene_0.add(main_window);
  
  //main_window.add(c.x(100).y(100).width(300).height(300).reference(main_window));
  
  sidebar = new Scene().x(0).y(350).c(color(255)).width(201).height(displayHeight-350);
  Window sidebar_window = new Window().x(0).y(0).c(color(100)).width(200).height(displayHeight-350).top(false).touchable(true).reference(sidebar);
  sidebar.add(sidebar_window);
  
  profile = new Scene().x(0).y(0).c(color(255)).width(201).height(350);
  Window profile_default = new Window().x(0).y(0).c(color(150)).width(200).height(350).top(false).touchable(true).reference(profile);
  Button_square save_button = new Button_square().x(200/2 + 45 -40).y(255 + 50).c(color(150,255,200)).width(80).height(30).name_end("window_save").reference(profile_default).complement(null);
  Button_square load_button = new Button_square().x(200/2 - 45 -40).y(255 + 50).c(color(150,255,200)).width(80).height(30).name_end("window_load").reference(profile_default).complement(null);
  Text button_name_save = new Text(40, 4, "Save", 20, "center", save_button);
  save_button.add_text(button_name_save);
  Text button_name_load = new Text(40, 4, "Load", 20, "center", load_button);
  load_button.add_text(button_name_load);
  profile_default.add(save_button);
  profile_default.add(load_button);
  
  profile.add(profile_default);
  
  current_scene.add(scene_0);
  current_scene.add(sidebar);
  current_scene.add(profile);
}
