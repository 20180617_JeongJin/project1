class Unit{
  protected String name;
  protected Book book;
  //protected ArrayList<Category> category;
  protected ArrayList<Unit> connection_to;
  protected ArrayList<Unit> connection_from;
  protected int x, y;
  
  protected ArrayList<PVector> connection_to_imsi;
  protected ArrayList<PVector> connection_from_imsi;
  
  protected boolean cleared;
  
  ArrayList<PVector> get_connection_to_imsi(){
    return this.connection_to_imsi;
  }
  
  Unit connection_to_imsi(ArrayList<PVector> connection_to_imsi){
    this.connection_to_imsi = connection_to_imsi;
    return this;
  }
  
  Unit(){
    this.cleared = false;
    
    this.book = new Book(null, this);
    
    connection_to = new ArrayList<Unit> ();
    connection_from = new ArrayList<Unit> ();
    
    connection_to_imsi = new ArrayList<PVector> ();
    connection_from_imsi = new ArrayList<PVector> ();
  }
  
  Unit name(String name){
    this.name = name;
    return this;
  }
  
  Unit book(Book book){
    this.book = book;
    return this;
  }
  
  Unit x(int x){
    this.x = x;
    return this;
  }
  
  Unit y(int y){
    this.y = y;
    return this;
  }
  
  Unit cleared(boolean cleared){
    this.cleared = cleared;
    return this;
  }
  
  void add_connection_to(Unit connection_to){
    this.connection_to.add(connection_to);
  }
  
  void delete_connection_to(Unit connection_to){
    this.connection_to.remove(connection_to);
  }
  
  void add_connection_from(Unit connection_from){
    this.connection_from.add(connection_from);
  }
  
  void delete_connection_from(Unit connection_from){
    this.connection_from.remove(connection_from);
  }
  
  boolean get_cleared(){
    return cleared;
  }
  
  int get_x(){
    return x;
  }
  
  int get_y(){
    return y;
  }
  
  String get_name(){
    return name;
  }
  
  Book get_book(){
    return book;
  }
  
  ArrayList<Unit> get_connection_to(){
    return this.connection_to;
  }
  
  ArrayList<Unit> get_connection_from(){
    return this.connection_from;
  }
  
  Unit_storage storage(){
    return new Unit_storage().unit(this);
  }
}

class Unit_storage extends Component{
  protected Unit unit;
  
  Unit_storage(){
  }
  
  Unit_storage unit(Unit unit){
    this.unit = unit;
    return this;
  }
  
  Unit get(){
    return unit;
  }
  
  void draw(){
  
  }
  
  void if_pressed(){
    
  }
  
  boolean is_pressed(){
    return false;
  }
  
  boolean is_on(){
    return false;
  }
}


class Unit_controller{
  Unit unit_to;
  Unit unit_from;

  Unit_controller(){
    unit_to = null;
    unit_from = null;
  }

  Unit_controller unit_to(Unit unit_to){
    this.unit_to = unit_to;
    return this;
  }
  
  Unit_controller unit_from(Unit unit_from){
    this.unit_from = unit_from;
    return this;
  }
  
  void connect(){
    if(unit_to==null || unit_from==null) return;
    
    unit_from.add_connection_to(unit_to);
    unit_to.add_connection_from(unit_from);
    unit_to=null;
    unit_from=null;
  }
  
  void disconnect(){
    if(unit_to==null || unit_from==null) return;
    
    unit_from.delete_connection_to(unit_to);
    unit_to.delete_connection_from(unit_from);
    unit_to=null;
    unit_from=null;
  }
}

class Unit_builder extends Component{
  protected String name;
  protected Book book;
  protected int x, y;
  protected boolean cleared;
  
  protected ArrayList<Unit> unit_to;
  protected ArrayList<Unit> unit_from;
  
  Unit_builder(){
    unit_to = new ArrayList<Unit>();
    unit_from = new ArrayList<Unit>();
    cleared = false;
    String name = "";
  }
  
  int get_x(){
    return this.x;
  }
  
  int get_y(){
    return this.y;
  }
  
  String get_name(){
    return this.name;
  }
  
  ArrayList<Unit> get_unit_to(){
    return this.unit_to;
  }
  
  ArrayList<Unit> get_unit_from(){
    return this.unit_from;
  }
  
  Unit_builder name(String name){
    this.name = name;
    return this;
  }
  
  Unit_builder book(Book book){
    this.book = book;
    return this;
  }
  
  Unit_builder x(int x){
    this.x = x;
    return this;
  }
  
  Unit_builder y(int y){
    this.y = y;
    return this;
  }
  
  Unit_builder cleared(boolean cleared){
    this.cleared = cleared;
    return this;
  }
  
  Unit_builder add_unit_to(Unit unit){
    this.unit_to.add(unit);
    return this;
  }
  
  Unit_builder add_unit_from(Unit unit){
    this.unit_from.add(unit);
    return this;
  }
  
  Unit build(){
    if(name == "") return null;
    
    Unit_controller unit_controller = new Unit_controller();
    
    Unit new_unit = new Unit();
    new_unit.name(name).book(new Book(null, new_unit)).x(x).y(y);
    
    for(Unit unit: unit_to){
      unit_controller.unit_to(unit).unit_from(new_unit).connect();
    }
    
    for(Unit unit: unit_from){
      unit_controller.unit_from(unit).unit_to(new_unit).connect();
    }
    
    units.add(new_unit);
    
    return new_unit;
  }
  
  boolean is_on(){
    return false;
  }
  
  void draw(){
  
  }
}

void loadUnits (String filename)
{
  File f = new File(dataPath(filename + ".json"));
  if(!f.exists()) return;
  
  current_map_name = filename;
  
  JSONObject json = loadJSONObject("data/" + filename + ".json");
  JSONArray units_get = json.getJSONArray("units");

  for (int i = 0; i < units_get.size(); i++) {
    
    JSONObject unit = units_get.getJSONObject(i);

    int x = unit.getInt("x");
    int y = unit.getInt("y");
    boolean cleared = unit.getBoolean("cleared");
    String name = unit.getString("name");
    map_editable = unit.getBoolean("map_editable");
    
    Unit imsi = new Unit().x(x).y(y).cleared(cleared).name(name);
    
    ArrayList<PVector> connection_to_imsi = new ArrayList<PVector>();
    ArrayList<PVector> connection_from_imsi = new ArrayList<PVector>();
    
    JSONArray connection_to = unit.getJSONArray("connection_to");
    for(int j=0; j<connection_to.size(); j++){
      JSONObject point = connection_to.getJSONObject(j); 
      int x_imsi = point.getInt("x");
      int y_imsi = point.getInt("y");
      connection_to_imsi.add(new PVector(x_imsi, y_imsi));
    }
    imsi.connection_to_imsi(connection_to_imsi);
    
    JSONArray connection_from = unit.getJSONArray("connection_from");
    for(int j=0; j<connection_from.size(); j++){
      JSONObject point = connection_from.getJSONObject(j); 
      int x_imsi = point.getInt("x");
      int y_imsi = point.getInt("y");
      connection_from_imsi.add(new PVector(x_imsi, y_imsi));
    }
    
    Book book = new Book(null, imsi);
    JSONArray book_get = unit.getJSONArray("book");
    for(int j=0; j<book_get.size(); j++){
      Page page = new Page(book);
      JSONArray page_get = book_get.getJSONArray(j);
      for(int k=0; k<page_get.size(); k++){
        JSONObject text = page_get.getJSONObject(k);
        String context = text.getString("context");
        String allign = text.getString("allign");
        int size = text.getInt("size");
        int width = text.getInt("width");
        int height = text.getInt("height");
        int x_get = text.getInt("x");
        int y_get = text.getInt("y");
        
        page.add(new Text(x_get, y_get, context, size, allign, page));
      }
      book.add(page);
    }
    imsi.book(book);
    
    units.add(imsi);
  }
  
  for(Unit unit: units){
    ArrayList<PVector> connection_data = unit.get_connection_to_imsi();
    for(PVector data : connection_data){
      for(Unit unit1: units){
        if(data.x==unit1.get_x() && data.y==unit1.get_y()){
          unit_controller.unit_from(unit);
          unit_controller.unit_to(unit1);
          unit_controller.connect();
        }
      }
    }
  }
}


void saveUnits (String filename)
{
  
  JSONArray values = new JSONArray();
  
  for(int i=0; i<units.size(); i++){
    JSONObject unit = new JSONObject();
    unit.setInt("x", (int)units.get(i).get_x());
    unit.setInt("y", (int)units.get(i).get_y());
    unit.setBoolean("cleared", (boolean)units.get(i).get_cleared());
    unit.setString("name", (String)units.get(i).get_name());
    unit.setBoolean("map_editable", (boolean)editable);
    
    JSONArray connection_to = new JSONArray();
    
    for(int j=0; j<units.get(i).get_connection_to().size(); j++){
      JSONObject point = new JSONObject();
      point.setInt("x", (int)units.get(i).get_connection_to().get(j).get_x());
      point.setInt("y", (int)units.get(i).get_connection_to().get(j).get_y());
      connection_to.setJSONObject(j, point);
    }
    
    unit.setJSONArray("connection_to", connection_to);
    
    JSONArray connection_from = new JSONArray();
    
    for(int j=0; j<units.get(i).get_connection_from().size(); j++){
      JSONObject point = new JSONObject();
      point.setInt("x", (int)units.get(i).get_connection_from().get(j).get_x());
      point.setInt("y", (int)units.get(i).get_connection_from().get(j).get_y());
      connection_from.setJSONObject(j, point);
    }
    
    unit.setJSONArray("connection_from", connection_from);
    
    JSONArray book = new JSONArray();
    
    if(units.get(i).get_book()!=null){
      for(int j=0; j<units.get(i).get_book().get_page().size(); j++){
        
        JSONArray page = new JSONArray();
        
         for(int k=0; k<units.get(i).book.get_page().get(j).get_page_component().size(); k++){
          JSONObject text = new JSONObject();
          text.setString("context", (String)units.get(i).get_book().get_page().get(j).get_page_component().get(k).get_context());
          text.setString("allign", (String)units.get(i).get_book().get_page().get(j).get_page_component().get(k).get_allign());
          text.setInt("size", (int)units.get(i).get_book().get_page().get(j).get_page_component().get(k).get_size());
          text.setInt("width", (int)units.get(i).get_book().get_page().get(j).get_page_component().get(k).get_width());
          text.setInt("height", (int)units.get(i).get_book().get_page().get(j).get_page_component().get(k).get_height());
          text.setInt("x", (int)units.get(i).get_book().get_page().get(j).get_page_component().get(k).get_x());
          text.setInt("y", (int)units.get(i).get_book().get_page().get(j).get_page_component().get(k).get_y());
          page.setJSONObject(k, text);
        }
        
        book.setJSONArray(j, page);
      }
    }
    
    unit.setJSONArray("book", book);
    
    values.setJSONObject(i, unit);
  }
  
  JSONObject json = new JSONObject();
  json.setJSONArray("units", values);

  saveJSONObject(json, "data/" + filename + ".json");
}

void exit()
{
  //saveUnits ("myunits");
}
