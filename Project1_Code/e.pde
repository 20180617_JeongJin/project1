class Map_free extends Component implements Pressable{
  protected int width, height, current_x, current_y;
  protected ArrayList<Component> component;
  protected boolean grid;
  
  protected String state;
  
  protected Unit_builder unit_builder;
  
  Map_free(){
    this.component = new ArrayList<Component>();
    this.state = "";
    
    this.component.add(new Window().x(0).y(0).width(100).height(100).c(color(255,0)).top(false).touchable(false).reference(this));
    this.component.add(new Window().x(0).y(0).width(100).height(100).c(color(255,0)).top(false).touchable(false).reference(this));
    this.component.add(new Window().x(0).y(0).width(100).height(100).c(color(255,0)).top(false).touchable(false).reference(this));
    Window w = new Window().x(0).y(0).width(100).height(100).c(color(255,0)).top(false).touchable(false).reference(this);
    //w.add(new Text_editor_controller().x(100).y(100).width(300).height(300).reference(w));
    this.component.add(w);
    
    unit_builder = new Unit_builder();
    
    grid = false;
    current_x=0;
    current_y=0;
  }
  
  Map_free state(String state){
    this.state = state;
    return this;
  }
  
  Map_free x(int x){
    this.x = x;
    return this;
  }
  
  Map_free y(int y){
    this.y = y;
    return this;
  }
  
  Map_free c(color c){
    this.c = c;
    return this;
  }
  
  Map_free width(int width){
    this.width = width;
    for(Component com: component){
      if(com instanceof Window){
        ((Window) com).width(width);
      }
    }
    return this;
  }
  
  Map_free height(int height){
    this.height = height;
    for(Component com: component){
      if(com instanceof Window){
        ((Window) com).height(height);
      }
    }
    return this;
  }
  
  Map_free reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Component get(int n){
    if(component.size()==0) return null;
    return this.component.get(n);
  }
  
  Map_free grid(boolean grid){
    this.grid = grid;
    return this;
  }
  
  void add(Component component){
    this.component.add(component);
  }
  
  void draw(){
    //println(unit_builder.get_name());
    //println(unit_builder.get_x());
    //println(unit_builder.get_y());
    //println(unit_builder.unit_from);
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    hint(ENABLE_DEPTH_TEST);
    //tint();
    if(maskImage==null) maskImage = createGraphics(displayWidth, displayHeight);
    //println(x, y, this.reference_delta().x, this.reference_delta().y, width, height, maskImage.width, maskImage.height);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1, 50);
    sourceImage.fill(this.c);
    sourceImage.rect(x, y, width, height);
    
    if(grid){
      for(int i=0; i<=width; i+=60){
        sourceImage.line(x+current_x%60+i,y,x+current_x%60+i,y+height);
      }
      for(int i=0; i<=height; i+=60){
        sourceImage.line(x,y+current_y%60+i,x+width,y+current_y%60+i);
      }
      
      for(int i=0; i<=width; i+=60){
        for(int j=0; j<=height; j+=60){
          sourceImage.fill(color(200));
          sourceImage.text(str(-current_x/60+i/60) + "," + str(-current_y/60+j/60), x+current_x%60+i,y+current_y%60+j);
        }
      }
    }
    sourceImage.endDraw();
    
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
    
    //additional function
    this.unit_sync();
    
    //state_machine.sync();
    
    for(int i=0; i<component.size(); i++){
      component.get(i).draw();
      //println(i);
    }
  }
  
  void add_from_0(int x, int y){
    //ellipse(x+reference_delta().x+20, y+reference_delta().y+20, (press_time-20)*2, (press_time-20)*2);
    if (((Window)this.component.get(3)).get_component().size()==0){
      ArrayList<Component> imsi = new ArrayList<Component>();
      ((Window)this.component.get(3)).component(imsi);
      ((Window)this.get(3)).add(new Button_circle().x((int)(x+20)).y((int)(y+20)).c(255).diameter(0).reference((Window)this.get(3)));
      
    }
    else{
      ((Button_circle)((Window)this.component.get(3)).get(0)).x((int)(x+20-(press_time-20))).y((int)(y+20-(press_time-20))).diameter((int)(press_time-20)*2);
    }
  }
  
  void add_from_1(int x, int y){
    //ellipse(x+reference_delta().x+20, y+reference_delta().y+20, (press_time-20)*2, (press_time-20)*2);
    //ArrayList<Component> imsi = new ArrayList<Component>();
    //((Window)this.component.get(3)).component(imsi);
    
    Button_circle button = ((Button_circle)((Window)this.component.get(3)).get(0));
    int imsi_x = mouseX-(int)reference_delta().x-this.x-20;
    int imsi_y = mouseY-(int)reference_delta().y-this.x-20;
    
    if(imsi_x>x){
      imsi_x = x+(imsi_x-x+30)/60*60;
    }else{
      imsi_x = x+(imsi_x-x-30)/60*60;
    }
    
    if(imsi_y>y){
      imsi_y = y+(imsi_y-y+30)/60*60;
    }else{
      imsi_y = y+(imsi_y-y-30)/60*60;
    }
    
    button.x(imsi_x).y(imsi_y).diameter(40);
    
    if(imsi_x-current_x>0) imsi_x += 60;
    if(imsi_y-current_y>0) imsi_y += 60;
    unit_builder.x((imsi_x-current_x)/60).y((imsi_y-current_y)/60);
    //fill(0);
    //text(str(unit_builder.get_x())+","+str(unit_builder.get_y()), mouseX, mouseY);
    //text(str((imsi_x-current_x)/60)+","+str((imsi_y-current_y)/60), mouseX, mouseY+20);
    //text(str((imsi_x)/60-(current_x)/60)+","+str((imsi_y)/60-(current_y)/60), mouseX, mouseY+40);
  }
  
  void add_from_2(Component get_component){
    //ellipse(x+reference_delta().x+20, y+reference_delta().y+20, (press_time-20)*2, (press_time-20)*2);
    unit_builder.add_unit_from(((Unit_storage)get_component).get());
    
    boolean already_placed = false;
    Unit imsi_unit = null;
    for(Unit unit: units){
      if(unit.get_x()==unit_builder.get_x()&&unit.get_y()==unit_builder.get_y()){
        ArrayList<Component> imsi = new ArrayList<Component>();
        ((Window)this.component.get(3)).component(imsi);
        imsi_unit = unit;
        already_placed = true;
        break;
      }
    }
    
    if(already_placed){
      if(unit_builder.get_unit_from().get(0).equals(imsi_unit)){
        
        this.unit_builder = new Unit_builder();
        return;
      }
      
      if(unit_builder.get_unit_from().get(0).get_connection_from().contains(imsi_unit)){
        Unit_controller unit_controller = new Unit_controller();
        unit_controller.unit_to(unit_builder.get_unit_from().get(0)).unit_from(imsi_unit).disconnect();
        
        this.unit_builder = new Unit_builder();
        return;
      }
      
      if(unit_builder.get_unit_from().get(0).get_connection_to().contains(imsi_unit)){
        Unit_controller unit_controller = new Unit_controller();
        unit_controller.unit_from(unit_builder.get_unit_from().get(0)).unit_to(imsi_unit).disconnect();
        
        this.unit_builder = new Unit_builder();
        return;
      }
      
      Unit_controller unit_controller = new Unit_controller();
      unit_controller.unit_from(unit_builder.get_unit_from().get(0)).unit_to(imsi_unit).connect();
      
      this.unit_builder = new Unit_builder();
      return;
      //println(1);
    }
    else{
      ArrayList<Component> imsi = new ArrayList<Component>();
      ((Window)this.component.get(3)).component(imsi);
      Window imsi_window = new Window().x(0).y(0).c(color(255,100)).width(displayWidth-200).height(displayHeight).close(true).touchable(true).reference((Window)component.get(3));
      Text_editor_controller text_edit_controller;
      text_edit_controller = new Text_editor_controller().x(width/2-150).y(height/2-150).width(300).height(300).reference(imsi_window).compliment(this.unit_builder.get_name());
      
      //unit_builder.name("11");
      imsi_window.add(text_edit_controller);
      Button_square imsi_button = new Button_square().x(width/2-50).y(height/2+130-20).c(color(150,255,200)).width(100).height(30).name_end("").reference(imsi_window).complement(text_edit_controller);
      Text button_name = new Text(50, 4, "Confirm", 20, "center", imsi_button);
      imsi_button.add_text(button_name);
      Button_chain chain0 = new Button_chain().name("unit_name").complement(this).reference(text_edit_controller);
      Button_chain chain1 = new Button_chain().name("").reference(imsi_button).complement(this);
      Button_chain chain2 = new Button_chain().name("close_window").reference(imsi_button).complement(imsi_window);
      imsi_button.add_chain(chain0);
      chain0.add_chain(chain1);
      chain1.add_chain(chain2);
      imsi_window.add(imsi_button);
      imsi_window.add(new Text(width/2, height/2-120, "Enter New Unit Name", 20, "center", imsi_window));
      ((Window)component.get(3)).add(imsi_window);
    }
  }
  
  void add_from_3(String name){
    this.unit_builder.name(name);
    for(Unit unit: units){
      if(unit.get_name().equals(name)) this.unit_builder.name("");
    }
    build_unit();
  }
  
  void build_unit(){
    if(!unit_builder.get_name().equals(""))this.unit_builder.build();
    reset_unit_builder();
  }
  
  void reset_unit_builder(){
    this.unit_builder = new Unit_builder();
  }
  
  void if_start_pressed(){
  
  }
  
  void if_while_pressed(){
    current_x += mouseX-pmouseX;
    current_y += mouseY-pmouseY;
  }
  
  void if_end_pressed(){
  
  }
  
  boolean is_on(){
    if(mouseX>this.reference_delta().x+x && mouseX<this.reference_delta().x+x+width
    && mouseY>this.reference_delta().y+y && mouseY<this.reference_delta().y+y+height){
      for(int i=0; i<component.size(); i++){
        boolean imsi = component.get(component.size()-i-1).is_on();
        if(imsi) return imsi;
      }
      mouse_on = this;
      
      return true;
    }
    return false;
  }
  
  int get_width(){
    return width;
  }
  
  int get_height(){
    return height;
  }
  
  void unit_sync(){
    //println(9);
    ArrayList<Component> imsi1 = new ArrayList<Component>();
    ((Window)this.component.get(0)).component(imsi1);
    ArrayList<Component> imsi2 = new ArrayList<Component>();
    ((Window)this.component.get(1)).component(imsi2);
    ArrayList<Component> imsi3 = new ArrayList<Component>();
    ((Window)this.component.get(2)).component(imsi3);
    
    for(Unit unit: units){
      if(unit.get_x()>=-current_x/60 && unit.get_x()<=-current_x/60+width/60 &&
      unit.get_y()>=-current_y/60 && unit.get_y()<=-current_y/60+height/60){
        int x_pos = (int)current_x%60+60*unit.get_x()+current_x/60*60;
        int y_pos = (int)current_y%60+60*unit.get_y()+current_y/60*60;
        color c = color(255,0,0);
        
        boolean is_studiable = true;
        for(Unit unit_from: unit.get_connection_from()){
          if(!unit_from.get_cleared()){
            is_studiable = false;
            break;
          }
        }
        if(unit.get_connection_from().size()==0 || is_studiable){
          c = color(255,255,0);
        }
        
        if(unit.get_cleared()) c = color(0,255,0);
        
        ((Window)this.get(1)).add(new Button_circle().x(x_pos-20).y(y_pos-20).c(c).diameter(40).name_while("unit_while").name_end("unit_end").reference((Window)this.get(1)).complement(unit.storage()));
        
        for(Unit connected_to: unit.get_connection_to()){
          ((Window)this.get(0)).add(new Line(x_pos, y_pos, x_pos + 60*(connected_to.get_x()-unit.get_x()), y_pos + 60*(connected_to.get_y()-unit.get_y()), (Window)this.get(0)));
        }
        
        for(Unit connected_from: unit.get_connection_from()){
          ((Window)this.get(0)).add(new Line(x_pos, y_pos, x_pos + 60*(connected_from.get_x()-unit.get_x()), y_pos + 60*(connected_from.get_y()-unit.get_y()), (Window)this.get(0)));
        }
        
        if((this.reference_delta().x+x+x_pos-mouseX)*(this.reference_delta().x+x+x_pos-mouseX)+
        (this.reference_delta().y+y+y_pos-mouseY)*(this.reference_delta().y+y+y_pos-mouseY)<20*20){
          ((Window)this.get(2)).add(new Unit_guide().name(unit.get_name()).context(null).x(x_pos).y(y_pos).to(unit.get_connection_to().size()).from(unit.get_connection_from().size()).reference((Window)this.get(2)));
        }
      }
    }
  }
}

class Unit_guide extends Component{
  protected String name;
  protected String context;
  protected int to, from;
  
  Unit_guide(){
  }
  
  Unit_guide name(String name){
    this.name = name;
    return this;
  }
  
  Unit_guide context(String context){
    this.context = context;
    return this;
  }
  
  Unit_guide x(int x){
    this.x = x;
    return this;
  }
  
  Unit_guide y(int y){
    this.y = y;
    return this;
  }
  
  Unit_guide to(int to){
    this.to = to;
    return this;
  }
  
  Unit_guide from(int from){
    this.from = from;
    return this;
  }
  
  Unit_guide reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  void draw(){
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    hint(ENABLE_DEPTH_TEST);
    //tint();
    if(maskImage==null) maskImage = createGraphics(0, 0);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.fill(color(255));
    sourceImage.rectMode(CENTER);
    sourceImage.rect(x, y-60, 70, 50);
    sourceImage.textAlign(CENTER);
    sourceImage.fill(color(0));
    if(name.length()>8) name = name.substring(0, 9) + "...";
    sourceImage.text(name + "\nto: " + to + "\nfrom: " + from, x, y-70);
    sourceImage.endDraw();
    
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
  }
  
  void if_start_pressed(){
    
  }
  
  boolean is_pressed(){
    return false;
  }
  
  boolean is_on(){
    return false;
  }
}
