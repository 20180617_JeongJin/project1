interface Effectable extends Interactable{
  //complement
}

interface Interactable{
  
}

interface Onable extends Interactable{
  abstract void if_on();
}

interface Pressable extends Interactable{
  abstract void if_start_pressed();
  abstract void if_while_pressed();
  abstract void if_end_pressed();
  //abstract void while_pressed();
}

abstract class Component{
  protected String name;
  protected int x, y;
  protected color c;
  protected Component reference;
  
  abstract void draw();
  
  Component name(String name){
    this.name = name;
    return this;
  }
  
  Component c(color c){
    this.c = c;
    return this;
  }
  
  Component x(int x){
    this.x = x;
    return this;
  }
  
  Component y(int y){
    this.y = y;
    return this;
  }
  
  Component reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Component get_reference(){
    return reference;
  }
  
  int get_x(){
   return this.x;
  }
  
  int get_y(){
   return this.y;
  }
  
  Component get_myself(){
    return this;
  }
  
  PVector reference_delta(){
    Component imsi_reference = reference;
    
    int x_reference=0;
    int y_reference=0;
    while(imsi_reference != null){
      x_reference += imsi_reference.get_x();
      y_reference += imsi_reference.get_y();
      imsi_reference = imsi_reference.get_reference();
    }
    
    return new PVector(x_reference, y_reference);
  }
  
  PGraphics get_maskimage(){
    if(reference instanceof Frame){
      PGraphics maskImage;
      maskImage = createGraphics(((Frame)reference).get_width(), ((Frame)reference).get_height());
      return maskImage;
    }
    
    if(reference instanceof Button_square){
      PGraphics maskImage;
      maskImage = createGraphics(((Button_square)reference).get_width(), ((Button_square)reference).get_height());
      return maskImage;
    }
    return null;
  }
  
  //abstract boolean is_pressed();
  abstract boolean is_on();
}
