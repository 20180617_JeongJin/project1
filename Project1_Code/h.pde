class Book extends Frame implements Pressable{
  protected Unit complement;
  protected ArrayList<Page> pages;
  
  protected int current_num;
  protected Page current_page;
  
  ArrayList<Page> get_page(){
    return this.pages;
  }
  
  Page get_current_page(){
    return current_page;
  }
  
  Page get(int n){
    return pages.get(n);
  }
  
  void add(Page page){
    pages.add(page.x(50).y(80));
    current_page = pages.get(current_num);
  }
  
  void remove_current(){
    Page page = current_page;
    pages.remove(page);
    current_num -= 1;
    if(current_num==-1) current_num=0;
    if(pages.size()==0){
      current_num=0;
      Page page_imsi = new Page(this);
      page_imsi.x(0).y(0).width(1920/2-100).height(1080/2-200);
      page_imsi.add(new Text(0, 0, complement.get_name(), 20, "left", page_imsi).width(1920/2-100).height(1080/2-200));
      this.add(page_imsi);
    }
    current_page = pages.get(current_num);
  }
  
  void add_previous(){
    Page page_imsi = new Page(this);
    page_imsi.x(0).y(0).width(1920/2-100).height(1080/2-200);
    page_imsi.add(new Text(0, 0, "New Page", 20, "left", page_imsi).width(1920/2-100).height(1080/2-200));
    this.add(page_imsi);
    
    //println(pages.size());
    
    ArrayList<Page> imsi_pages = new ArrayList<Page>();
    for(int i=0; i<pages.size()-1; i++){
      if(i==current_num){
        imsi_pages.add(page_imsi);
      }
      imsi_pages.add(pages.get(i));
    }
    pages = imsi_pages;
    current_page = pages.get(current_num);
    //println(pages.size());
  }
  
  void add_next(){
    Page page_imsi = new Page(this);
    page_imsi.x(0).y(0).width(1920/2-100).height(1080/2-200);
    page_imsi.add(new Text(0, 0, "New Page", 20, "left", page_imsi).width(1920/2-100).height(1080/2-200));
    this.add(page_imsi);
    
    //println(pages.size());
    
    ArrayList<Page> imsi_pages = new ArrayList<Page>();
    for(int i=0; i<pages.size()-1; i++){
      imsi_pages.add(pages.get(i));
      if(i==current_num){
        imsi_pages.add(page_imsi);
      }
    }
    
    pages = imsi_pages;
    current_num += 1;
    current_page = pages.get(current_num);
  }
  
  Book width(int width){
    this.width = width;
    for(Page page: pages){
      page.width(1920/2-100);
    }
    return this;
  }
  
  Book height(int height){
    this.height = height;
    for(Page page: pages){
      page.height(1080/2-200);
    }
    return this;
  }
  
  Book reference(Component reference){
    this.reference = reference;
    return this;
  }
  
  Book(Component reference, Unit complement){
    this.c = color(255);
    this.reference = reference;
    this.complement = complement;
    this.pages = new ArrayList<Page>();
    this.current_num = 0;
    this.x =0;
    this.y =0;
  }
  
  void nextPage(){
    if(current_num<pages.size()-1) current_num +=1;
    if(current_num>pages.size()-1) current_num = pages.size()-1;
    current_page = pages.get(current_num);
  }
  
  void previousPage(){
    if(current_num>0) current_num -=1;
    if(current_num<0) current_num = 0;
    current_page = pages.get(current_num);
  }
  
  void draw(){
    if(current_page!=null)current_page.draw();
  }
  
  void if_start_pressed(){
  }
  
  void if_while_pressed(){
  }
  
  void if_end_pressed(){
  }
  
  boolean is_on(){
    if(mouseX>this.reference_delta().x+x && mouseX<this.reference_delta().x+x+width
    && mouseY>this.reference_delta().y+y && mouseY<this.reference_delta().y+y+height){
      boolean imsi = false;
      if(current_page!=null) imsi = current_page.is_on();
      if(imsi) return imsi;
      mouse_on = this;
      return true;
    }
    return false;
  }
}

class Page extends Frame implements Pressable{
  protected int current_y;
  
  protected ArrayList<Text> page_component;
  
  ArrayList<Text> get_page_component(){
    return page_component;
  }
  
  Page(Component reference){
    current_y = 0;
    this.c = color(255);
    this.reference = reference;
    this.page_component = new ArrayList<Text>();
  }
  
  int get_width(){
    return this.width;
  }
  
  int get_height(){
    return this.height;
  }
  
  void add(Text n){
    page_component.add(n);
  }
  
  //void add(Page_sortable n){
  //  page_component.add(n);
  //}
  
  Page width(int width){
    this.width = width;
    return this;
  }
  
  Page height(int height){
    this.height = height;
    return this;
  }
  
  Page x(int x){
    this.x = x;
    return this;
  }
  
  Page y(int y){
    this.y = y;
    return this;
  }
  
  void draw(){
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    hint(ENABLE_DEPTH_TEST);
    //tint();
    if(maskImage==null) maskImage = createGraphics(displayWidth, displayHeight);
    //println(x, y, this.reference_delta().x, this.reference_delta().y, width, height, maskImage.width, maskImage.height);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1, 0);
    sourceImage.fill(this.c);
    sourceImage.rect(x, y, width, height);
    sourceImage.endDraw();
    
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
    
    for(int i=0; i<page_component.size(); i++){
      page_component.get(i).draw(current_y);
      //println(i);
    }
  }
  
  void if_start_pressed(){
    
  }
  
  void if_while_pressed(){
    current_y += mouseY-pmouseY;
    if(current_y>0) current_y=0;
  }
  
  void if_end_pressed(){
  
  }
  
  boolean is_on(){
    if(mouseX>this.reference_delta().x+x && mouseX<this.reference_delta().x+x+width
    && mouseY>this.reference_delta().y+y && mouseY<this.reference_delta().y+y+height){
      mouse_on = this;
      return true;
    }
    return false;
  }
}

interface Page_sortable{
  void draw(int delta);
}

class Text extends Component implements Page_sortable{
  private String context;
  private String allign;
  private int size;
  
  private int width, height;
  
  
  int get_size(){
    return size;
  }
  
  Text(int x, int y, String context, int size, String allign, Component reference){
    this.x = x;
    this.y = y;
    this.context = context;
    this.reference = reference;
    this.allign = allign;
    this.size = size;
    
    this.width=0;
    this.height=0;
  }
  
  Text context(String context){
    this.context = context;
    return this;
  }
  
  int get_width(){
    return width;
  }
  
  int get_height(){
    return height;
  }
  
  Text width(int width){
    this.width = width;
    return this;
  }
  
  Text height(int height){
    this.height = height;
    return this;
  }
  
  void draw(){
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    hint(ENABLE_DEPTH_TEST);
    //tint();
    if(maskImage==null) maskImage = createGraphics(0, 0);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.fill(this.c);
    sourceImage.textSize(size);
    if(allign=="center") sourceImage.textAlign(CENTER);
    if(allign=="right") sourceImage.textAlign(RIGHT);
    if(allign=="left") sourceImage.textAlign(LEFT);
    if(width==0 || height==0) sourceImage.text(context, x, y+size);
    else sourceImage.text(context, x, y, width, height);
    sourceImage.endDraw();
    
    //sourceImage.mask(maskImage);
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
  }
  
  void draw(int delta_y){
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    hint(ENABLE_DEPTH_TEST);
    //tint();
    if(maskImage==null) maskImage = createGraphics(displayWidth, displayHeight);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.fill(this.c);
    sourceImage.textSize(size);
    if(allign=="center") sourceImage.textAlign(CENTER);
    if(allign=="right") sourceImage.textAlign(RIGHT);
    if(allign=="left") sourceImage.textAlign(LEFT);
    sourceImage.text(context, x, y+delta_y+size);
    sourceImage.endDraw();
    
    //sourceImage.mask(maskImage);
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
  }
  
  boolean is_on(){
    return false;
  }
  
  void if_start_pressed(){
    
  }
  
  boolean is_pressed(){
    return false;
  }
  
  String get_context(){
    return this.context;
  }
  
  String get_allign(){
    return this.allign;
  }
}

//class Blank implements Page_sortable{
//  void draw(){
  
//  }
//}

//class Image implements Page_sortable{
//  void draw(){
  
//  }
//}

class Line extends Component implements Page_sortable{
  private int x1, y1, x2, y2;
  
  Line(int x1, int y1, int x2, int y2, Component reference){
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.reference = reference;
  }
  
  void draw(){
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    hint(ENABLE_DEPTH_TEST);
    //tint();
    if(maskImage==null) maskImage = createGraphics(displayWidth, displayHeight);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.fill(this.c);
    sourceImage.line(x1,y1,x2,y2);
    sourceImage.endDraw();
      
    //sourceImage.mask(maskImage);
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
  }
  
  void draw(int delta_y){
    PGraphics sourceImage;
    PGraphics maskImage = get_maskimage();
    
    hint(ENABLE_DEPTH_TEST);
    //tint();
    if(maskImage==null) maskImage = createGraphics(0, 0);
    sourceImage = createGraphics(maskImage.width, maskImage.height);
    sourceImage.beginDraw();
    sourceImage.stroke(1);
    sourceImage.fill(this.c);
    sourceImage.line(x1,y1+delta_y,x2,y2+delta_y);
    sourceImage.endDraw();
      
    //sourceImage.mask(maskImage);
    image(sourceImage, this.reference_delta().x, this.reference_delta().y);
  }
  
  boolean is_on(){
    return false;
  }
  
  void if_start_pressed(){
    
  }
  
  boolean is_pressed(){
    return false;
  }
}
